$(document).ready( function() {

	// collapses sa typeface contents
	$('.content').hide();

	// add click listener
	$('.box').on("click", "h1", function() {

		// toggles content expansion visibility
		$(this).siblings().filter('.content').toggle();
	});

});